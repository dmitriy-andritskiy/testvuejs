import { namespace } from 'vuex-class';
import { Base } from '@/mixins/base';

const windowEventListenerStore = namespace('windowEventListener');
const applicationStore = namespace('application');

export class Page extends Base {
    @windowEventListenerStore.Getter('getKeyCode') public getKeyCode!: number;
    @windowEventListenerStore.Mutation('restoreKeyEvent') public restoreKeyEvent!: () => void;

    @applicationStore.Mutation('setLoader') private setLoader!: (status: boolean) => void;

    public async loader(data: any): Promise<void> {
        this.setLoader(true);
        await Promise.resolve(data);
        this.setLoader(false);
    }
}
