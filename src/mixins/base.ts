import Vue from 'vue';

import SERVICE_IDENTIFIER from '@/models/Identifiers';
import container from '@/DI';
import { IApplicationService } from '@/services/IApplicationServices';

export class Base extends Vue {
    public service = container.get<IApplicationService>(SERVICE_IDENTIFIER.ApplicationService);
}
