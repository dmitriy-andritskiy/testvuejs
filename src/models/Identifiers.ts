const SERVICE_IDENTIFIER = {
    ApplicationService: Symbol('ApplicationService'),
    CONTAINER: Symbol('Container'),
};

export default SERVICE_IDENTIFIER;
