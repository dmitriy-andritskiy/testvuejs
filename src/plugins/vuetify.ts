import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark: true,
        options: {
            customProperties: true,
        },
        themes: {
            light: {
                background: '#dddddd',
                loaderDot: 'blue',
            },
            dark: {
                background: '#333333',
                loaderDot: 'white',
            },
        },
    },
});
