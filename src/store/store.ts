import Vue from 'vue';
import Vuex from 'vuex';
import application from './modules/application';
import authenticate from './modules/authenticate';
import windowEventListener from './modules/windowEventListener';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        application,
        authenticate,
        windowEventListener,
    },
});
