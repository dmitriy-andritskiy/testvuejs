// tslint:disable no-empty-interface
export interface IRootState {}

export interface IApplicationState {
    isLoadingPage: boolean;
}

export interface IAuthencticateState {
    token: string | null;
    authenticateSource: any;
}

export interface IWindowEventListenerState {
    keyEvent: KeyboardEvent | null;
}
