import { IAuthencticateState, IRootState } from '@/store/interfaces/interfaces';
import { GetterTree, MutationTree, ActionTree } from 'vuex';
import axios from 'axios';
import Vue from 'vue';

const TOKEN: string = 'token';

const state: IAuthencticateState = {
    token: localStorage.getItem(TOKEN),
    authenticateSource: null,
};

const getters: GetterTree<IAuthencticateState, IRootState> = {
    getToken(state): string | null {
        return state.token;
    },

    getAuthenticateSource(state): any {
        return state.authenticateSource;
    },
};

const mutations: MutationTree<IAuthencticateState> = {
    setToken(state, data: string | null): void {
        state.token = data;
        if (data) {
            localStorage.setItem(TOKEN, data);
        } else {
            localStorage.removeItem(TOKEN);
        }
    },

    setAuthenticateSource(state, data: any): void {
        state.authenticateSource = data;
    },

    cancelAuthenticateSourcee(state): void {
        if (state.authenticateSource) {
            state.authenticateSource.cancel();
        }
    },
};

const actions: ActionTree<IAuthencticateState, IRootState> = {
    async login({ commit, state }: any, data: any): Promise<boolean> {
        let promiseResult = false;
        await axios
            .post('', data)
            .then((result: any) => {
                commit('setToken', result.data.token);
                promiseResult = true;
            })
            .catch((error: any) => {
                Vue.toasted.error('login');
            });
        return promiseResult;
    },

    async forgotPassword({ commit, state }: any, data: any): Promise<boolean> {
        let promiseResult = false;
        await axios
            .post('', data)
            .then((result: any) => {
                promiseResult = true;
            })
            .catch((error: any) => {
                Vue.toasted.error('forgotPassword');
            });
        return promiseResult;
    },

    async resetPassword({ commit, state }: any, data: any): Promise<boolean> {
        let promiseResult = false;
        await axios
            .post('', data)
            .then((result: any) => {
                promiseResult = true;
            })
            .catch((error: any) => {
                Vue.toasted.error('login');
            });
        return promiseResult;
    },

    async validateLink({ commit, state }: any, data: any): Promise<boolean> {
        let promiseResult = false;
        await axios
            .post('', data)
            .then((result: any) => {
                promiseResult = true;
            })
            .catch((error: any) => {
                Vue.toasted.error('login');
            });
        return promiseResult;
    },

    async recoveryPassword({ commit, state }: any, data: any): Promise<boolean> {
        let promiseResult = false;
        await axios
            .post('', data)
            .then((result: any) => {
                promiseResult = true;
            })
            .catch((error: any) => {
                Vue.toasted.error('login');
            });
        return promiseResult;
    },
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};
