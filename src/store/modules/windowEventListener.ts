import { ActionTree, GetterTree, MutationTree } from 'vuex';
import { IWindowEventListenerState, IRootState } from '../interfaces/interfaces';

const state: IWindowEventListenerState = {
    keyEvent: null,
};

const getters: GetterTree<IWindowEventListenerState, IRootState> = {
    getKeyCode(state: IWindowEventListenerState): number {
        if (state.keyEvent !== null) {
            return state.keyEvent.keyCode;
        }
        return 0;
    },
};

const mutations: MutationTree<IWindowEventListenerState> = {
    setUpKeyEvent(state: IWindowEventListenerState, data: KeyboardEvent) {
        state.keyEvent = data;
    },

    restoreKeyEvent(state: IWindowEventListenerState) {
        state.keyEvent = null;
    },
};

const actions: ActionTree<IWindowEventListenerState, IRootState> = {};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};
