import { IApplicationState, IRootState } from '@/store/interfaces/interfaces';
import { GetterTree, MutationTree, ActionTree } from 'vuex';
import axios from 'axios';
import Vue from 'vue';

const state: IApplicationState = {
    isLoadingPage: false,
};

const getters: GetterTree<IApplicationState, IRootState> = {
    getLoader(state: IApplicationState): boolean {
        return state.isLoadingPage;
    },
};

const mutations: MutationTree<IApplicationState> = {
    setLoader(state: IApplicationState, data: boolean): void {
        state.isLoadingPage = data;
    },
};

const actions: ActionTree<IApplicationState, IRootState> = {};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};
