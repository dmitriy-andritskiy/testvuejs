import { Container } from 'inversify';
import 'reflect-metadata';
import SERVICE_IDENTIFIER from '@/models/Identifiers';

import { IApplicationService } from '@/services/IApplicationServices';
import ApplicationService from '@/services/ApplicationService';

const container: Container = new Container();

container.bind<IApplicationService>(SERVICE_IDENTIFIER.ApplicationService).to(ApplicationService);

export default container;
