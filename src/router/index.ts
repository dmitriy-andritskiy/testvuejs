import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Meta from 'vue-meta';
import Main from '@/views/Main.vue';
import store from '@/store/store';

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location: any): any {
    return (originalPush.call(this, location) as any).catch((error: any) => error);
};

Vue.use(VueRouter);
Vue.use(Meta);

const routes: RouteConfig[] = [
    {
        path: '/',
        name: '',
        meta: {
            public: false,
        },
        component: Main,
        children: [
            {
                path: '/',
                name: 'Template',
                meta: {
                    public: false,
                },
                component: () => import(/* webpackChunkName: "index" */ '@/views/template/index.vue'),
            },
            {
                path: '/about',
                name: 'About',
                meta: {
                    public: false,
                },
                component: () => import(/* webpackChunkName: "index" */ '@/views/about/index.vue'),
            },
        ],
    },
    {
        path: '/login',
        name: 'Login',
        meta: {
            public: true,
        },
        component: () => import(/* webpackChunkName: "index" */ '@/views/login/index.vue'),
    },
    {
        path: '/forgot-password',
        name: 'ForgotPasword',
        meta: {
            public: true,
        },
        component: () => import(/* webpackChunkName: "index" */ '@/views/forgotPassword/index.vue'),
    },
    {
        path: '/reset-password',
        name: 'ResetPassword',
        meta: {
            public: true,
        },
        component: () => import(/* webpackChunkName: "index" */ '@/views/resetPassword/index.vue'),
    },
    {
        path: '/recovery-password',
        name: 'RecoveryPassword',
        meta: {
            public: true,
        },
        component: () => import(/* webpackChunkName: "index" */ '@/views/recoveryPassword/index.vue'),
    },
    {
        path: '/update-browser',
        name: 'UpdateBrowser',
        meta: {
            public: true,
        },
        component: () => import(/* webpackChunkName: "index" */ '@/views/updateBrowser/index.vue'),
    },
    {
        path: '/page-not-found',
        name: 'PageNotFound',
        meta: {
            public: true,
        },
        component: () => import(/* webpackChunkName: "index" */ '@/views/pageNotFound/index.vue'),
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

router.beforeEach((to, from, next) => {
    if (to.matched.length > 0) {
        if (to.matched.some((record: any) => !record.meta.public)) {
            if (store.getters['authenticate/getToken'] === null) {
                next({
                    path: '/login',
                    params: { nextUrl: to.fullPath },
                });
            } else {
                if (store.getters['authenticate/getAuthenticateSource']) {
                    store.dispatch('authenticate/cancelRequests');
                }
                next();
            }
        } else {
            next();
        }
    } else {
        next({
            path: '/page-not-found',
            params: { nextUrl: '/page-not-found' },
        });
    }
});

export default router;
