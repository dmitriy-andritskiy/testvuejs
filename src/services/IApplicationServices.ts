import TemplateService from '@/services/base/TemplateService';
import BrowserService from '@/services/base/BrowserService';

export interface IApplicationService {
    TemplateService: TemplateService;
    BrowserService: BrowserService;
}
