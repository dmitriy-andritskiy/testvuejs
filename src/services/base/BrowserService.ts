import { BaseService } from '@/services/BaseService';
import IUserBrowser from '@/services/interfaces/UserBrowser';
import supportedBrowsers from '@/core/const/supportedBrowsers';

class BrowserService extends BaseService {
    /**
     * Check user browser
     * @returns IUserBrowser
     * @memberof BrowserService
     */
    // tslint:disable
    public checkUserBrowser(): IUserBrowser {
        let ua = navigator.userAgent,
            tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return {
                name: 'IE',
                version: tem[1] || '',
            };
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
            if (tem != null) {
                return {
                    name: tem[1].replace('OPR', 'Opera'),
                    version: tem[2],
                };
            }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) {
            M.splice(1, 1, tem[1]);
        }
        return {
            name: M[0],
            version: parseInt(M[1]),
        };
    }
    // tslint:enable

    /**
     * Check is supported user browser
     * @returns boolean
     * @memberof BrowserService
     */
    public isSupportedUserBrowser(): boolean {
        const browser = this.checkUserBrowser();
        if (
            (browser.name.toUpperCase() === supportedBrowsers[0].name.toUpperCase() && browser.version >= supportedBrowsers[0].version) ||
            (browser.name.toUpperCase() === supportedBrowsers[1].name.toUpperCase() && browser.version >= supportedBrowsers[1].version) ||
            (browser.name.toUpperCase() === supportedBrowsers[2].name.toUpperCase() && browser.version >= supportedBrowsers[2].version) ||
            (browser.name.toUpperCase() === supportedBrowsers[3].name.toUpperCase() && browser.version >= supportedBrowsers[3].version) ||
            (browser.name.toUpperCase() === supportedBrowsers[4].name.toUpperCase() && browser.version >= supportedBrowsers[4].version)
        ) {
            return true;
        } else {
            return false;
        }
    }
}

export default BrowserService;
