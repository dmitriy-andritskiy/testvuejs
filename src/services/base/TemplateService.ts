import { BaseService } from '@/services/BaseService';

class TemplateService extends BaseService {
    /**
     * Get
     * @returns string[]
     * @memberof TemplateService
     */
    public getTemplateService(): string {
        return 'Hello world from Template Service!';
    }
}

export default TemplateService;
