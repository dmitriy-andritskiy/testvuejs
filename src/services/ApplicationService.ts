import { injectable } from 'inversify';
import { IApplicationService } from '@/services/IApplicationServices';
import TemplateService from '@/services/base/TemplateService';
import BrowserService from '@/services/base/BrowserService';

@injectable()
export default class ApplicationService implements IApplicationService {
    private templateService: TemplateService | null = null;
    private browserService: BrowserService | null = null;

    public get TemplateService(): TemplateService {
        if (this.templateService === null) {
            this.templateService = new TemplateService();
        }

        return this.templateService;
    }

    public get BrowserService(): BrowserService {
        if (this.browserService === null) {
            this.browserService = new BrowserService();
        }

        return this.browserService;
    }
}
