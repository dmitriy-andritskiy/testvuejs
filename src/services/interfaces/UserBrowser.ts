interface IUserBrowser {
    name: string;
    version: number | string;
}

export default IUserBrowser;
