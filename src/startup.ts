import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/store';
import vuetify from './plugins/vuetify';

export default class Startup {
    /**
     * Build project, entry point for correct vue
     */
    public async build(): Promise<void> {
        this.initGlobals();
        this.initVue();
    }

    /**
     * Init vue
     *
     * @private void
     * @memberof Startup
     */
    private initVue(): void {
        new Vue({
            router,
            store,
            vuetify,
            render: (h: any) => h(App),
        }).$mount('#app');
    }

    private initGlobals() {
        // Global comps
        // tslint:disable-next-line:no-var-requires
        require('./configs/global-components-config');
    }
}
