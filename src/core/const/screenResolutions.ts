const screenResolutions: any[] = [
    { name: 'HD', width: 1280, height: 720 },
    { name: 'FULL HD', width: 1920, height: 1080 },
    { name: '2K', width: 2560, height: 1440 },
    { name: '4K', width: 3840, height: 2160 },
];

export default screenResolutions;
