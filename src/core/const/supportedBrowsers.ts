const supportedBrowsers: any[] = [
    { name: 'Chrome', version: 55 },
    { name: 'Edge', version: 17 },
    { name: 'Firefox', version: 50 },
    { name: 'Opera', version: 45 },
    { name: 'Safari', version: 9 },
    { name: 'Yandex', version: 20 },
];

export default supportedBrowsers;
