import IBaseFilter from '@/core/interfaces/filters/base/baseFilter';

interface ITemplateFilter extends IBaseFilter {
    search: string;
}

export default ITemplateFilter;
