interface IBaseFilter {
    skip: number;
    take: number;
}

export default IBaseFilter;
