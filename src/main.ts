import Vue from 'vue';
import './registerServiceWorker';

Vue.config.productionTip = false;

// axios
// tslint:disable-next-line:no-var-requires
require('./configs/axios-config');

// Config Vue libs
// tslint:disable-next-line:no-var-requires
require('./configs/libs-config');

// toasted
// tslint:disable-next-line:no-var-requires
import Toasted from 'vue-toasted';
Vue.use(Toasted, { position: 'bottom-left', duration: 5000 });

import Startup from './startup';
const startup = new Startup();
Promise.resolve(startup.build());
